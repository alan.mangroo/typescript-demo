let id : number = 5
console.log('ID: ', id)

let company: string = "Atlassian"
let isPublished: boolean = true
let x: any = "Hello"
let age: number

let ids: number[] = [1,2,3,4]
ids.push(5)

let arr: any[] = [1,2,3, true, "hello"]

// Tuple
let person: [number, string, boolean] = [1, 'Alan', true]

// Tuple Array
let employee: [number, string][]

employee = [
    [1,'Alan'],
    [2,'aaa'],
    [3,'bbbb'],
]

// Union
let pid: string | number = 22
pid = 'hello'

// enum
enum Direction1 {
    UP = 1,
    DOWN,
    LEFT,
    RIGHT
}
console.log(Direction1.LEFT)

enum Direction2 {
    UP = 'up',
    DOWN = 'down',
    LEFT = 'left',
    RIGHT = 'right'
}
console.log(Direction2.LEFT)

// Objects
type User = {
    id: number,
    name: string
}

const user: User = {
    id: 1,
    name: 'john'
}

// Type Assertion
let cid: any = 1
// let customerId = <number>cid
let customerId = cid as number
customerId = 123

// Functions
function addNum(x: number, y: number): number {
    return x+y
}
console.log(addNum(4,3))

function log(message: string|number): void {
    console.log(message)
}
log("Alan Log")


// Interfaces
interface UserInterface {
    readonly id: number,
    name: string,
    age?: number
}

const user1: UserInterface = {
    id: 1,
    name: 'john'
}

type Point = number | string
const p1: Point = 1

interface MathFunc {
    (x: number, y: number): number
}
const add: MathFunc = (x: number, y: number): number => x+y
const sub: MathFunc = (x: number, y: number): number => x-y


interface PersonInterface {
    id: number,
    name: string
    register(): string
}

//Classes
class Person implements PersonInterface {
    id: number
    name: string

    constructor(id: number, name: string) {
        console.log("Constructor")
        this.id = id
        this.name = name
    }

    register() {
        return `${this.name} is now registered`
    }
}

const alan = new Person(1, 'alan')
const toby = new Person(2, 'toby')
console.log(alan, toby)
console.log(alan.register())

//Subclass
class Employee extends Person {
    position: string

    constructor(id: number, name: string, position: string) {
        super(id, name)
        this.position = position
    }
}

const emp = new Employee(3, 'dominic', 'dev')
console.log(emp.register())

// Generics
function getArray<T>(items: T[]) : T[] {
    return new Array().concat(items)
}

let numArray = getArray<number>([1,2,3,4])
let strArray = getArray<string>(['b','a','d'])

// numArray.push('hello') // Not valid
numArray.push(123)


